System robot

/*
 * ---------------------------------
 * Environment
 * ---------------------------------
 */
Event environment: environment(X)				// X=ok|notok

Event temperature: temperature(X)				// X = temperature (c)

/*
 * ---------------------------------
 * Robot-adapter
 * ---------------------------------
 */
Dispatch robotAdapterAdd: robot(T,A)			// T=Type ; A=args

Event robotCmd: cmd(X)
Dispatch robotCmdPriority: cmd(X)

/*
 * Robot position
 */
Event robotSonar: robotSonar(distance(X))		// X = distance
Event robotSonarObstacle: obstacle(X)           // From robot virtual sonar; X = name

/*
 * ---------------------------------
 * Mind
 * ---------------------------------
 */
Dispatch cmdStop: cmdStop
Dispatch cmdGoHome: cmdGoHome
Dispatch cmdExplore: cmdExplore
Dispatch cmdReachBomb: cmdReachBomb

Dispatch handleBag: handleBag(X)							// handling bag logic; X = action 
Dispatch bag: bag(picture(X))								// X=photo

/*
 * Robot position
 */
Dispatch robotState: state(position(X,Y), movement(M))		// (X,Y) = coordinates ; M = w|a|s|d|h
Dispatch robotHome: robotHome
Dispatch robotNearBag: robotNearBag

/*
 * ---------------------------------
 * Console
 * ---------------------------------
 */
Dispatch alert: alert
Dispatch bagStatus: bagStatus(X, args(Y))					// X = bomb|bag ; Y = arguments

// GUI
Event usercmd: usercmd(X)									//from robot GUI;  X=robotgui(CMD) CMD=s(low)

Context ctxRobot ip [ host="localhost" port=8079 ] -httpserver

QActor world_observer context ctxRobot {
	
	Rules {
		// Impostazioni default:
		temperature(18).
		// Regole di valutazione:
		evaluateTemp :- temperature(X), eval(lt, X, 25).	// R-TempOk
		// Regola di valutazione complessiva:
		evaluateEnvironment :- evaluateTemp.
	}
	
	State doObserve initial [
		[ !? evaluateEnvironment ] emit environment: environment(ok)
			else emit environment: environment(notok)
	] transition
		whenTime 1000 -> doObserve
		whenEvent temperature: temperature(X) do
			ReplaceRule temperature(Z) with temperature(X)	// R-TempOk
		finally repeatPlan
}

QActor robot_adapter context ctxRobot {

	Rules {
//		robotType("robotSerial", setuparg("/dev/ttyACM0")).
//		robotType("robotSerial", setuparg("/dev/rfcomm0")).
//		robotType("robotVirtual", setuparg("localhost,true")).
		robotType("robotVirtual", setuparg("localhost,false")).
//		robotType("robotDemo", setuparg("")).
	}
	
	State init initial [
		println("Robot adapter init")
	] transition
		whenTime 100 -> initRobots
	
	State initRobots [
		[ ?? robotType(T, setuparg(A))] selfMsg robotAdapterAdd: robot(T,A)
	] transition
		whenTime 2000 -> doWork
		whenMsg robotAdapterAdd: robot(T,A) do javaRun it.unibo.robotadapter.allRobots.setUp(T,A)
		finally repeatPlan
	
	State doWork [
		
	] transition
		stopAfter 60000
		whenMsg robotCmdPriority -> executeCommand,
		whenEvent robotCmd -> executeCommand
		finally repeatPlan
	
	State executeCommand resumeLastPlan [
		onMsg robotCmdPriority: cmd(X) -> javaRun it.unibo.robotadapter.allRobots.doMove(X);
		onEvent robotCmd: cmd(X) -> javaRun it.unibo.robotadapter.allRobots.doMove(X)
	]
}

QActor robot_discovery_mind context ctxRobot {
	
	Rules {
		environment(notok).
	}
	
	/*
     * ---------------------------------
	 * Evaluate environment conditions before starting
     * ---------------------------------
	 */
	State home initial [
		onMsg robotHome: robotHome -> forward console -m robotHome: robotHome
	] transition
		stopAfter 60000
		whenEvent environment: environment(E) do
			demo replaceRule(environment(X), environment(E)),
		whenMsg [ !? environment(ok) ] cmdExplore -> goToExploration  // R-explore
		finally repeatPlan
	
	/*
     * ---------------------------------
	 * Exploration business logic
     * ---------------------------------
	 */
	State goToExploration [
		forward robot_adapter -m robotCmdPriority: cmd(blinkStart)	  // R-blinkLed (start)
	] transition
		whenTime 100 -> exploration
	
	State exploration [
		println("EXPLORING ...");	// TODO implement logic
		emit robotCmd: cmd(w)		// TODO debug
	] transition
		stopAfter 60000
		whenMsg cmdStop -> goToIdle,						// R-stopExplore
		whenMsg robotNearBag -> goToHandleBag
	
	State goToHandleBag [
		selfMsg handleBag: handleBag(halt);					// R-stopAtBag
		selfMsg handleBag: handleBag(takePhoto)				// R-takePhoto
	] transition
		whenTime 10 -> handleBag
	
	State handleBag [
		println("HANDLING BAG ...");
		onMsg handleBag: handleBag(halt) -> {				// R-stopAtBag
			emit robotCmd: cmd(h);
			emit robotCmd: cmd(blinkStop)
		};
		onMsg handleBag: handleBag(takePhoto) -> {
			println("TAKE PHOTO ...");	// TODO implement	// R-takePhoto
			forward console -m bag: bag(picture(X))			// R-sendPhoto
		}
	] transition
		whenTime 1000 -> handleBag
		whenMsg handleBag -> handleBag,
		whenMsg cmdGoHome -> returnHome,
		whenMsg cmdExplore -> goToExploration
	
	State goToIdle [
		forward robot_adapter -m robotCmdPriority: cmd(h);
		forward robot_adapter -m robotCmdPriority: cmd(blinkStop) // R-blinkLed (stop)
	] transition
		whenTime 100 -> idle
	
	State idle [
		
	] transition
		stopAfter 60000
		whenMsg cmdExplore -> exploration,					// R-continueExplore
		whenMsg cmdGoHome -> returnHome						// R-backHome
	
	State returnHome [
		println("GOING HOME...")	// TODO implement logic
	] transition
		stopAfter 60000
		whenMsg robotHome -> home
}

QActor robot_retriever_mind context ctxRobot {
	
	Rules {
		environment(notok).
	}
	
	State home initial [
		
	] transition
		stopAfter 60000
		whenEvent environment: environment(E) do
			demo replaceRule(environment(X), environment(E)),
		whenMsg [ !? environment(ok) ] cmdReachBomb -> goToReachBomb
            // R-reachBag
		finally repeatPlan
	
	/*
     * ---------------------------------
	 * Retrieval business logic
     * ---------------------------------
	 */
	State goToReachBomb [
		
	] transition
		whenTime 1000 -> reachBomb
	
	State reachBomb [
		println("RETRIVING BOMB ...")
	] transition
		whenTime 3000 -> reachBomb
		whenMsg robotHome -> home
}

QActor console context ctxRobot {
	
	State init initial [
		println("Console init")
	] transition
		whenTime 100 -> doWork
	
	State doWork [
		
	] transition
		stopAfter 60000
		whenEvent usercmd -> adaptCommand,
		whenEvent robotState -> updateView,					// R-consoleUpdate
		whenMsg bag -> handlePhoto
		finally repeatPlan
	
	State adaptCommand resumeLastPlan [
		printCurrentEvent;
		onEvent usercmd: usercmd(robotgui(cmd(explore))) ->
            forward robot_discovery_mind -m cmdExplore: cmdExplore;
		onEvent usercmd: usercmd(robotgui(cmd(halt))) ->
            forward robot_discovery_mind -m cmdStop: cmdStop;
		onEvent usercmd: usercmd(robotgui(cmd(home))) ->
            forward robot_discovery_mind -m cmdGoHome: cmdGoHome;
		onEvent usercmd: usercmd(robotgui(bagStatus(bomb))) ->
            selfMsg bagStatus: bagStatus(bomb, args(picture(nothing)));
		onEvent usercmd: usercmd(robotgui(bagStatus(bag))) ->
            selfMsg bagStatus: bagStatus(bag, args(nothing))
	]
	
	State handlePhoto [
		onMsg bag: bag(picture(X)) -> printCurrentMessage
	] transition
		whenTime 3000 -> handlePhoto
		whenMsg bagStatus -> handleBagStatus,
		whenEvent usercmd -> adaptCommand
		finally repeatPlan
	
	State handleBagStatus [
		onMsg bagStatus: bagStatus(bomb, args(picture(X))) -> {
			// TODO implement save X						       // R-storePhoto
			forward robot_discovery_mind -m cmdGoHome: cmdGoHome;  // R-backHomeSinceBomb
			selfMsg alert: alert								   // R-alert
		};
		onMsg bagStatus: bagStatus(bag, Y) ->
			forward robot_discovery_mind -m cmdExplore: cmdExplore
                // R-continueExploreAfterPhoto
	] transition
		whenTime 3000 -> doWork
		whenMsg alert -> handleAlert
	
	State handleAlert [
		
	] transition
		whenTime 3000 -> handleAlert
		whenMsg robotHome: robotHome do
			forward robot_retriever_mind -m cmdReachBomb: cmdReachBomb
                // R-whaitForHome & R-reachBag
	
	State updateView resumeLastPlan [
		onMsg robotState: state(X) -> printCurrentEvent
	]
}